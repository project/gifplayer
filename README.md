# Gif Player Field

Gif Player Field creates a simple file field types that allows you to upload
the GIF files and configure the output for this using the Field Formatters.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/gifplayer).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/gifplayer).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No other modules are required.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

You can configure the cover image for GIF or add GifPlayer library globally so
this can be used on custom theme/modules.


## Maintainers

- OPTASY -  [optasy](https://www.drupal.org/u/optasy)
- Adrian ABABEI - [web247](https://www.drupal.org/u/web247)
- Daniel Rodriguez -[danrod](https://www.drupal.org/u/danrod)
- skaught-1 - (https://www.drupal.org/u/skaught-1)
