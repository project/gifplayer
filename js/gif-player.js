/**
 * @file
 * Provides JavaScript for Inline Entity Form.
 */

(function ($) {
  'use strict';

  var initialized;

  function applyGifPlayer() {
    if (!initialized) {
      initialized = true;

      $('.gifplayer').each(function () {
        $(this).gifplayer();
      });
    }
  }

  Drupal.behaviors.gifPlayer = {
    attach: function (context, settings) {

      // Apply gifPlayer.
      applyGifPlayer();
    }
  };

})(jQuery);
