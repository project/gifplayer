IMPORTANT
===================

The Gif library assets are added temporary in the module as we can't add
external libraries by declaring repositories recursively on the module
composer.json


Opened issue on the drupal-composer page:
https://github.com/drupal-composer/drupal-project/issues/278
