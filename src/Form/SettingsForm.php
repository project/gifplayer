<?php

namespace Drupal\gifplayer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a form that configures gifplayer settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The core entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager) {

    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gifplayer_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gifplayer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('gifplayer.settings');

    $form = ['#attributes' => ['enctype' => 'multipart/form-data']];

    $form['library_attach'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Attach GifPlayer library globaly.'),
      '#description' => $this->t(
        'Check it to load GifPlayer library globally. By default this is
        attached only to the GifPlayer field and can be used only inside forms
        the field type is used.'
      ),
      '#default_value' => $config->get('library_attach') ?: FALSE,
    ];

    $form['file_upload_details'] = [
      '#markup' => $this->t(
        '<b>File will be used as cover for the GIF player fields.</b>'
      ),
    ];

    $form['cover_png'] = [
      '#type' => 'managed_file',
      '#name' => 'cover_png',
      '#title' => $this->t('Png cover'),
      '#size' => 20,
      '#description' => $this->t(
        'File used as cover on GIF player fields. Leave this empty to use the
        module default file. PNG formats only.'
      ),
      '#default_value' => ($config->get('cover_png') === 0) ?:
      [$config->get('cover_png')],
      '#upload_validators' => ['png'],
      '#upload_location' => 'public://my_files/',
    ];

    // Add hidden value of default FID of cover PNG if this has been added.
    $form['cpid'] = [
      '#type' => 'hidden',
      '#value' => $config->get('cover_png'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $cid = (int) reset($values['cover_png']);
    $cpid = (int) $values['cpid'];

    // Set file status permanent.
    if ($cid !== 0 && $file = $this->loadFile($cid)) {
      $file->setPermanent();
      $file->save();
    }

    // Delete previously uploaded file if that has been changed.
    if ($cid !== $cpid && $file = $this->loadFile($cpid)) {
      $file->delete();
    }

    // Save the updated settings.
    $this->config('gifplayer.settings')
      ->set('library_attach', $values['library_attach'])
      ->set('cover_png', $cid)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Load given id file.
   *
   * @param int $fid
   *   The file id.
   *
   * @return object|bool
   *   The file object or boolean.
   */
  protected function loadFile(int $fid) {

    // Load file and return this if exists.
    if ($file = $this->entityTypeManager->getStorage('file')->load($fid)) {
      return $file;
    }

    return FALSE;
  }

}
