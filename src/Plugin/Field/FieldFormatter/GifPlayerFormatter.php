<?php

namespace Drupal\gifplayer\Plugin\Field\FieldFormatter;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Implementation of Gif Player formatter.
 *
 * @FieldFormatter(
 *   id = "gif_player_formatter",
 *   label = @Translation("Gif Player"),
 *   field_types = {
 *     "gifplayer"
 *   }
 * )
 */
class GifPlayerFormatter extends EntityReferenceFormatterBase {

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Construct a new \Drupal\gifplayer\Plugin\Field\FieldFormatter\GifPlayerFormatter object.
   *
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The file URL generator object.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    FileUrlGenerator $fileUrlGenerator,
    ModuleExtensionList $extension_list_module,
    ConfigFactoryInterface $configFactory
    ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->extensionListModule = $extension_list_module;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('file_url_generator'),
      $container->get('extension.list.module'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['label'] = [
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 12,
      '#title' => $this->t('Label'),
      '#description' => $this->t(
        'A value for the label in the circle over the image.'
      ),
      '#default_value' => $this->getSettings()['label'],
      '#required' => TRUE,
    ];

    $elements['wait'] = [
      '#type' => 'select',
      '#title' => $this->t('Wait'),
      '#options' => [
        'true' => $this->t('True'),
        'false' => $this->t('False'),
      ],
      '#description' => $this->t(
        'Would you wait until the animation file has been fully loaded to play,
        or start playing right away.'
      ),
      '#default_value' => $this->getSettings()['wait'],
      '#required' => TRUE,
    ];

    $elements['playon'] = [
      '#type' => 'select',
      '#title' => $this->t('PlayOn'),
      '#options' => [
        'hover' => $this->t('Hover'),
        'click' => $this->t('Click'),
      ],
      '#description' => $this->t(
        'Event that triggers playing the animated gif.'
      ),
      '#default_value' => $this->getSettings()['playon'],
      '#required' => TRUE,
    ];

    $elements['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#options' => [
        'gif' => 'Gif',
      ],
      '#description' => $this->t(
        'Load an animated gif file.'
      ),
      '#default_value' => $this->getSettings()['mode'],
      '#required' => TRUE,
    ];

    $elements['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSettings()['width'],
      '#required' => TRUE,
    ];

    $elements['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSettings()['height'],
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary['label'] = [
      '#markup' => $this->t(
        'Label: @label', ['@label' => $this->getSettings()['label']]
      ),
    ];

    $summary['wait'] = [
      '#markup' => $this->t(
        'Wait: @wait', ['@wait' => $this->getSettings()['wait']]
      ),
    ];

    $summary['playon'] = [
      '#markup' => $this->t(
        'PlayOn: @playon', ['@playon' => $this->getSettings()['playon']]
      ),
    ];

    $summary['mode'] = [
      '#markup' => $this->t(
        'Mode: @mode', ['@mode' => $this->getSettings()['mode']]
      ),
    ];

    $summary['width'] = [
      '#markup' => $this->t(
        'Width: @width', ['@width' => $this->getSettings()['width']]
      ),
    ];

    $summary['height'] = [
      '#markup' => $this->t(
        'Height: @height', ['@height' => $this->getSettings()['height']]
      ),
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'label' => 'gif',
      'wait' => 'false',
      'playon' => 'click',
      'mode' => 'gif',
      'width' => '250',
      'height' => '300',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if (count($items) <= 0) {
      return [];
    }

    // Get the field formatter settings.
    $settings = $this->getSettings();

    // Load imagcover from settings if this is set.
    // @codingStandardsIgnoreStart
    $cid = ($this->configFactory->get('gifplayer.settings')->get('cover_png') !== NULL) ? $this->configFactory->get('gifplayer.settings')->get('cover_png') : 0;

    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    // @codingStandardsIgnoreEnd

    // Set image cover path.
    file_put_contents("/tmp/cid", json_encode($cid));
    if ($cid !== 0 && $file = $file_storage->load($cid)) {
      $img_cover = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    }
    else {
      $img_cover = $this->fileUrlGenerator->generateAbsoluteString($this->extensionListModule->getPath('gifplayer') . '/img/banana.png');
    }

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $item = $file->_referringItem;

      $elements[$delta] = [
        '#theme' => 'gif_player',
        '#gif_label' => $settings['label'],
        '#playon' => $settings['playon'],
        '#wait' => $settings['wait'],
        '#mode' => $settings['mode'],
        '#width' => $settings['width'],
        '#height' => $settings['height'],
        '#gif' => $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri()),
        '#img_cover' => $img_cover,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];

      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $elements[$delta] += ['#attributes' => []];
        $elements[$delta]['#attributes'] += $item->_attributes;

        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    // Attach library to the field, checks if this is not added globally.
    // @codingStandardsIgnoreStart
    $global_attach = \Drupal::config('gifplayer.settings')
      ->get('library_attach');
    // @codingStandardsIgnoreEnd

    if (!$global_attach) {
      $elements['#attached'] = ['library' => 'gifplayer/gifplayer'];
    }

    return $elements;
  }

}
