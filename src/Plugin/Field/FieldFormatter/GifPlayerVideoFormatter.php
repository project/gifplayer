<?php

namespace Drupal\gifplayer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implementation of Gif Player formatter.
 *
 * @FieldFormatter(
 *   id = "gif_player_video_formatter",
 *   label = @Translation("Gif Player Video"),
 *   field_types = {
 *     "gifplayer_video"
 *   }
 * )
 */
class GifPlayerVideoFormatter extends GifPlayerFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#options' => [
        'video' => $this->t('Video'),
      ],
      '#description' => $this->t(
        'Load an animated video file.'
      ),
      '#default_value' => $this->getSettings()['mode'],
      '#required' => TRUE,
      '#disabled' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'label' => 'gif',
      'wait' => 'false',
      'playon' => 'click',
      'mode' => 'video',
      'width' => '250',
      'height' => '300',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Early opt-out if the field is empty.
    if (count($items) <= 0) {
      return [];
    }

    // Get the field formatter settings.
    $settings = $this->getSettings();

    // Load imagcover from settings if this is set.
    // @codingStandardsIgnoreStart
    $cid = \Drupal::config('gifplayer.settings')->get('cover_png');

    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    // @codingStandardsIgnoreEnd

    // Set image cover path.
    if ($cid !== 0 && $file = $file_storage->load($cid)) {
      $img_cover = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    }
    else {
      $img_cover = \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::service('extension.list.module')->getPath('gifplayer') . '/img/banana.png');
    }

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $item = $file->_referringItem;

      $elements[$delta] = [
        '#theme' => 'gif_player_video',
        '#gif_label' => $settings['label'],
        '#playon' => $settings['playon'],
        '#wait' => $settings['wait'],
        '#mode' => $settings['mode'],
        '#width' => $settings['width'],
        '#height' => $settings['height'],
        '#video' => \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri()),
        '#img_cover' => $img_cover,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];

      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $elements[$delta] += ['#attributes' => []];
        $elements[$delta]['#attributes'] += $item->_attributes;

        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    // Attach library to the field, checks if this is not added globally.
    $global_attach = \Drupal::config('gifplayer.settings')
      ->get('library_attach');

    if (!$global_attach) {
      $elements['#attached'] = ['library' => 'gifplayer/gifplayer'];
    }

    return $elements;
  }

}
