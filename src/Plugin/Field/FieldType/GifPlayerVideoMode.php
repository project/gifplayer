<?php

namespace Drupal\gifplayer\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface as FSI;

/**
 * Provides a field type of Gif Player.
 *
 * @FieldType(
 *   id = "gifplayer_video",
 *   label = @Translation("Gif Player Video"),
 *   module = "gifplayer",
 *   category = @Translation("Reference"),
 *   description = @Translation("Gif Player Video field"),
 *   default_formatter = "gif_player_video_formatter",
 *   default_widget = "gif_player_widget",
 *   serialized_property_names = {
 *     "settings"
 *   },
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class GifPlayerVideoMode extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'file',
      'display_field' => TRUE,
      'display_default' => TRUE,
      'uri_scheme' => \Drupal::config('system.file')->get('default_scheme'),
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'file_extensions' => 'webm mp4',
      'file_directory' => '[date:custom:Y]-[date:custom:m]',
      'max_filesize' => '',
      'description_field' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FSDI $field_definition) {
    return [
      'columns' => [
        'target_id' => [
          'description' => 'The ID of the WebM/Mp4 file entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'display' => [
          'description' => 'Flag to control whether this WebM/Mp4 file should be
            displayed when viewing content.',
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'default' => 1,
        ],
        'description' => [
          'description' => 'A description of the WebM/Mp4 file.',
          'type' => 'text',
        ],
      ],
      'indexes' => [
        'target_id' => ['target_id'],
      ],
      'foreign keys' => [
        'target_id' => [
          'table' => 'file_managed',
          'columns' => ['target_id' => 'fid'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FSDI $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['display'] = DataDefinition::create('boolean')
      ->setLabel(t('Display'))
      ->setDescription(t(
        'Flag to control whether this WebM/Mp4 file should be displayed when
        viewing content'
      ));

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('WebM/Mp4 file Description'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FSI $form_state) {
    $element = [];
    $settings = $this->getSettings();

    $element = parent::fieldSettingsForm($form, $form_state);

    // Make the extension list a little more human-friendly by comma-separation.
    $extensions = str_replace(' ', ', ', $settings['file_extensions']);

    $element['file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#default_value' => $extensions,
      '#description' => $this->t(
        'The extensions for this upload is restrincted to WebM or Mp4 file
        types.'
      ),
      '#element_validate' => [[get_class($this), 'validateExtensions']],
      '#weight' => 1,
      '#maxlength' => 256,
      '#required' => TRUE,
      '#disabled' => TRUE,
    ];

    return $element;
  }

}
