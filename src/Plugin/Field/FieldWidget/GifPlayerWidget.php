<?php

namespace Drupal\gifplayer\Plugin\Field\FieldWidget;

use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'gifplayer' widget.
 *
 * @FieldWidget(
 *   id = "gif_player_widget",
 *   module = "gifplayer",
 *   label = @Translation("Gif Player"),
 *   field_types = {
 *     "gifplayer",
 *     "gifplayer_video"
 *   }
 * )
 */
class GifPlayerWidget extends FileWidget {}
